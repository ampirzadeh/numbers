import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import vuetify from './plugins/vuetify'
import VueScrollTo from 'vue-scrollto'

// offset 64 + 11 extra pixels because of the appbar
Vue.use(VueScrollTo, { offset: -75 })

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
