export function sumOfDigits(num: number | string): number {
  // Check if input is number or string if number, convert it ot string then proceed
  // Create an array of number's digits them sum up the array with reduce one-liner
  return typeof num === 'number'
    ? num
        .toString()
        .split('')
        .map((digit: number | string) => parseInt(digit as string))
        .reduce((a: number, b: number) => a + b, 0)
    : num
        .split('')
        .map((digit: number | string) => parseInt(digit as string))
        .reduce((a: number, b: number) => a + b, 0)
}
export function gcd(num1: number, num2: number): number {
  // Recursive GCD
  return num1 === 0 ? num2 : gcd(num2 % num1, num1)
}
export function coprime(num: number): number[] {
  const coprimes: number[] = []

  for (let i = 1; i < num; i++) {
    // If gcd is 1 add i to coprimes
    gcd(i, num) === 1 && coprimes.push(i)
  }

  return coprimes
}
export function primeFactors(num: number, unique?: boolean): number[] {
  const primeFactors: number[] = []
  for (let i = 2; i <= num; i++) {
    while (num % i === 0) {
      primeFactors.push(i)
      num /= i
    }
  }
  return unique ? [...new Set(primeFactors)] : primeFactors
}
export function allPositiveDivisors(num: number): number[] {
  const divisors: number[] = []
  for (let i = 1; i <= Math.floor(Math.sqrt(num)); i += 1)
    if (num % i === 0) {
      divisors.push(i)
      if (num / i !== i) divisors.push(num / i)
    }
  divisors.sort((x, y) => x - y) // numeric sort
  return divisors
}
export function say(num: number, short: boolean): string {
  const ones: string[] = [
    '',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
  ]
  const tens: string[] = [
    '',
    '',
    'twenty',
    'thirty',
    'forty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety',
  ]
  const teens: string[] = [
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
  ]

  function convertTens(num: number): string {
    return num < 10
      ? ones[num]
      : num >= 10 && num < 20
      ? teens[num - 10]
      : tens[Math.floor(num / 10)] +
        (num % 10 === 0 ? ' ' : '-') +
        ones[num % 10]
  }
  function convertHundreds(num: number): string {
    return num > 99 && !short
      ? `${ones[Math.floor(num / 100)]} hundred ${convertTens(num % 100)}`
      : short
      ? num.toString()
      : convertTens(num)
  }
  function convertThousands(num: number): string {
    return num >= 1000
      ? `${convertHundreds(Math.floor(num / 1000))} thousand ${convertHundreds(
          num % 1000
        )}`
      : convertHundreds(num)
  }

  function convertMillions(num: number): string {
    return num >= 1000000
      ? `${convertMillions(
          Math.floor(num / 1000000)
        )} million ${convertThousands(num % 1000000)}`
      : convertThousands(num)
  }

  function convertBillions(num: number): string {
    return num >= 1000000000
      ? `${convertBillions(
          Math.floor(num / 1000000000)
        )} billion ${convertMillions(num % 1000000000)}`
      : convertMillions(num)
  }

  function convertTrillions(num: number): string {
    return num >= 1000000000000
      ? `${convertTrillions(
          Math.floor(num / 1000000000000)
        )} trillion ${convertBillions(num % 1000000000000)}`
      : convertBillions(num)
  }

  function convertQuadrillions(num: number): string {
    return num >= 1000000000000000
      ? `${convertQuadrillions(
          Math.floor(num / 1000000000000000)
        )} quadrillion ${convertTrillions(num % 1000000000000000)}`
      : convertTrillions(num)
  }

  return num === 0 ? 'zero' : convertQuadrillions(num).replace(/\s{2,}/g, ' ')
}
export function isPrime(num: number): boolean {
  if (num === 2) return true
  const boundary: number = Math.floor(Math.sqrt(num))
  for (let i = 2; i <= boundary; i++) if (num % i === 0) return false
  return num >= 2
}
export function digitalRoot(num: number): number {
  // Recursive digital root with previously made sumOfDigits function
  return num < 10 ? num : digitalRoot(sumOfDigits(num))
}
export function engineeringNotation(
  num: number
): { base: number | string; power: number } {
  let result: { base: number | string; power: number }
  num.toString().length > 3
    ? (result = {
        base: parseFloat(
          num
            .toString()
            .replace(
              num.toString().slice(num.toString().length % 3),
              `.${num.toString().slice(num.toString().length % 3)}`
            )
        ),
        power: num.toString().slice(num.toString().length % 3).length,
      })
    : (result = {
        base: num,
        power: 0,
      })
  return result
}
export function countOccurrences(nums: number[]): { [key: string]: number[] } {
  const result: { [key: string]: number[] } = {}
  // Check if input is array.
  if (nums instanceof Array) {
    // Create an array for non-existing property and fill existing arrays.
    nums.forEach((key, index) => {
      // !result[key] ? (result[key] = [index]) : result[key].push(index)
      if (!result[key]) {
        // Initial object property creation.
        result[key] = [index] // Create an array for that property.
      } else {
        // Same occurrences found.
        result[key].push(index) // Fill the array.
      }
    })
  }
  return result
}
export function liouvilleLambda(num: number): number {
  // if even number of prime factors returns 1, otherwise returns -1
  return primeFactors(num, false).length % 2 === 0 ? 1 : -1
}
export function radical(num: number): number {
  // all prime factors multiplied
  return primeFactors(num, true).reduce(
    (totalValue, currentValue) => totalValue * currentValue
  )
}
export function sumOfDivisors(num: number, aliquot: boolean): number {
  return allPositiveDivisors(num).reduce((totalValue, currentValue) =>
    !aliquot
      ? totalValue + currentValue
      : currentValue !== num
      ? totalValue + currentValue
      : 0
  )
}
export function harmonicMean(num: number): number {
  let sum = 0
  allPositiveDivisors(num).map((n) => (sum += 1 / n))

  return allPositiveDivisors(num).length / sum
}
export function divisibility(
  num: number
): { number: number; remainder: number }[] {
  const result: { number: number; remainder: number }[] = []
  for (let n = 2; n < 10; n++) {
    result.push({
      number: n,
      remainder: num % n,
    })
  }
  return result
}
